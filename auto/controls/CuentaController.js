'use strict'
const { body, validationResult, check } = require('express-validator');
//const { where } = require('sequelize');
//const { sequelize } = require('../models/');
const models = require('../models/');
const bcrypt = require('bcrypt');
const saltRounds = 11;
let jwt = require("jsonwebtoken");
var cuent = models.cuenta;
//const { token } = require('morgan');

class CuentaController {
    async sesion(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty) {
            var login = await cuent.findOne({
                where: { correo: req.body.email },
                include: { model: models.cuenta, as: "persona", attributes: ['apellidos'] }
            });
            if (login === null) {
                res.status(400);
                res.json({ msg: "Cuenta no encontrada...", code: 400 });
            } else {
                //res.estatus(200);
                var isClaveValida = function (clave, claveUser) {
                    return bcrypt.compareSync(claveUser, clave);
                };
                if (login.estado) {
                    if (isClaveValida(login.clave, req.body.clave)) {
                        const tokenData = {
                            externa: login.external_id,
                            email: login.correo,
                            check: true
                        };
                        require("dotenv").config();
                        const llave = process.env.KEY;
                        const env = process.env.KEY;
                        const token = jwt.sign(tokenData, llave, {
                            expiresIn: '1h'
                        });
                        res.json({
                            msg: "OK...",
                            token: token,
                            user: login.persona + '' + login.persona.apellidos,
                            code: 200
                        });
                    } else {
                        res.json({ msg: "Clave Incorrecta...", code: 200 });
                    }
                } else {
                    res.json({ msg: "Datos no coinsiden", code: 200 });
                }
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos no encontrados", code: 400, errors: errors });
        }

    }
}
module.exports = CuentaController;