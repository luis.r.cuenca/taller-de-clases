'use strict'
const { body, validationResult, check } = require('express-validator');
//const { sequelize } = require('../models/');
var models = require('../models');
//variables
var auto = models.auto;
var marca = models.marcas;
const bcrypt = require('bcrypt');
const saltRounds = 11;

//const { Transaction, ValidationError } = require('sequelize');
class AutoController {

    //Listar datos de auto
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await auto.findOne({
            where: { external_id: external },
            //include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['modelo', 'anio', 'cilindraje', 'color', 'placa', 'precio']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: 'OK.!', code: 200, info: lista });
    }

    // Guardar los datos de auto
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty) {
            let marca_id = req.body.external_marca;
            if (marca_id != undefined) {
                let marcaAux = await marca.findOne({ where: { external_id: marca_id } });
                console.log(marcaAux);
                if (marcaAux) {
                    var data = {
                        modelo: req.body.modelo,
                        anio: req.body.anio,
                        cilindraje: req.body.cilindraje,
                        color: req.body.color,
                        placa: req.body.placa,
                        precio: req.body.precio,
                        //Envia el ID de la marca
                        id_marca: marcaAux.id
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await auto.create(data, transaction);
                        console.log('guardado');
                        await transaction.commit();
                        res.json({ msg: "Auto registrado con éxito...", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0], message) {
                            res.json({ msg: error.errors[0], message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    //Guarda los datos de auto
                    res.status(400);
                    res.json({ msg: "Marca no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }
    // Modificar los datos de auto
    /*async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var aut = await aut.findOne({ where: { external_id: req.body.external } });
            if (aut === null) {
                //TODO
                res.status(400);
                res.json({ msg: "No exixte el registro...", code: 400 });
            } else {
                aut.identificacion = req.body.dni;
                aut.tipo_identificacion = req.body.tipo;
                aut.nombres = req.body.nombres;
                aut.apellidos = req.body.dapellidos;
                aut.telefono = req.body.telefono;
                aut.direccion = req.body.direccion;
                aut.external_id = uuid.v4;
                var result = await aut.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se he podido modificar los datos...", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Datos modificados correctamente...", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }

    }*/
}
module.exports = AutoController;