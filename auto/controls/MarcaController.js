'use strict'
const { body, validationResult, check } = require('express-validator');
//const { sequelize } = require('../models/');
var models = require('../models');
//variables
var marca = models.marcas;
const bcrypt = require('bcrypt');
const saltRounds = 11;

//const { Transaction, ValidationError } = require('sequelize');
class MarcaController {

    //Listar datos de marca
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await marca.findOne({
            where: { external_id: external },
            //include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['nombre', 'anio', 'fechaLanzamiento']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: 'OK.!', code: 200, info: lista });
    }

    // Guardar los datos de marca
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty) {
                var data = {
                    nombre: req.body.nombre,
                    anio: req.body.anio,
                    fechaLanzamiento: req.body.fechaLanzamiento
                };
                res.status(200);
                let transaction = await models.sequelize.transaction();
                try {
                    await marca.create(data, transaction);
                        console.log('guardado');
                    await transaction.commit();
                    res.json({ msg: "Marca registrada con éxito...", code: 200 });
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    if (error.errors && error.errors[0], message) {
                        res.json({ msg: error.errors[0], message, code: 200 });
                    } else {
                        res.json({ msg: error.message, code: 200 });
                    }
                }
            } else {
                res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    // Modificar los datos de marca
    /*async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var aut = await aut.findOne({ where: { external_id: req.body.external } });
            if (aut === null) {
                //TODO
                res.status(400);
                res.json({ msg: "No exixte el registro...", code: 400 });
            } else {
                aut.identificacion = req.body.dni;
                aut.tipo_identificacion = req.body.tipo;
                aut.nombres = req.body.nombres;
                aut.apellidos = req.body.dapellidos;
                aut.telefono = req.body.telefono;
                aut.direccion = req.body.direccion;
                aut.external_id = uuid.v4;
                var result = await aut.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se he podido modificar los datos...", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Datos modificados correctamente...", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }

    }*/
}
module.exports = MarcaController;