'use strict'
const { body, validationResult, check } = require('express-validator');
//const { sequelize } = require('../models/');
var models = require('../models/');
//variables
var rol = models.rol;
var persona = models.persona;
var cuenta = models.cuenta;
//var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 11;

//const { Transaction, ValidationError } = require('sequelize');
class PersonaController {
    //Listar datos de persona
    

    async obtener(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external },
            include: { model: models.cuenta, as: "cuenta", attributes: ['correo'] },
            attributes: ['nombres', 'apellidos', 'identificacion', 'tipo_identificacion', 'direccion']
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ msg: 'OK.!', code: 200, info: lista });
    }

    // Guardar los datos de persona
    async guardar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty) {
            let rol_id = req.body.external_rol;
            if (rol_id != undefined) {
                let rolaux = await rol.findOne({ where: { external_id: rol_id } });
                console.log(rolaux);
                if (rolaux) {
                    var claveHash = function (clave) {
                        return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);
                    }
                    var data = {
                        identificacion: req.body.dni,
                        tipo_identificacion: req.body.tipo,
                        nombres: req.body.nombres,
                        apellidos: req.body.dapellidos,
                        telefono: req.body.telefono,
                        direccion: req.body.direccion,
                        id_rol: rolaux.id,
                        cuenta: {
                            usuario: req.body.email,
                            clave: claveHash(req.body.clave)
                        }
                    };
                    res.status(200);
                    let transaction = await models.sequelize.transaction();
                    try {
                        await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                        console.log('guardado');
                        await transaction.commit();
                        res.json({ msg: "Datos registrados con éxito...", code: 200 });
                    } catch (error) {
                        if (transaction) await transaction.rollback();
                        if (error.errors && error.errors[0], message) {
                            res.json({ msg: error.errors[0], message, code: 200 });
                        } else {
                            res.json({ msg: error.message, code: 200 });
                        }
                    }
                } else {
                    //Guarda los datos de persona
                    res.status(400);
                    res.json({ msg: "Datos no encontrados", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Faltan datos", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "Datos faltantes", code: 400, errors: errors });
        }
    }

    // Modificar los datos de persona
    async modificar(req, res) {
        let errors = validationResult(req);
        if (errors.isEmpty()) {
            var person = await person.findOne({ where: { external_id: req.body.external } });
            if (person === null) {
                //TODO
                res.status(400);
                res.json({ msg: "No exixte el registro...", code: 400 });
            } else {
                person.identificacion = req.body.dni;
                person.tipo_identificacion = req.body.tipo;
                person.nombres = req.body.nombres;
                person.apellidos = req.body.dapellidos;
                person.telefono = req.body.telefono;
                person.direccion = req.body.direccion;
                person.external_id = uuid.v4;
                var result = await person.save();
                if (result === null) {
                    res.status(400);
                    res.json({ msg: "No se he podido modificar los datos...", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Datos modificados correctamente...", code: 200 });
                }
            }
        } else {
            req.status(400);
            res.json({ msg: "Datos faltantes", code: 400 });
        }

    }
}
module.exports = PersonaController;