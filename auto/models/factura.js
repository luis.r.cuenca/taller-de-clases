
'use strict'
module.exports=(sequelize, DataTypes)=>{
    //Datos para guardar en la entidad
    const factura= sequelize.define('factura',{
        descripcion: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
        descuento: {type: DataTypes.BOOLEAN, defaultValue: true},
        cantidad: {type: DataTypes.INTEGER(), defaultValue: 0},
        subtotal: {type: DataTypes.DOUBLE(), defaultValue: 0.0},
        iva: {type: DataTypes.DOUBLE(), defaultValue: 0.0},
        total: {type: DataTypes.DOUBLE(), defaultValue: 0.0},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        external_id:{type: DataTypes.UUID, defaultValue: DataTypes.UUID}
    },{freezeTableName: true});

    //Relaciones con la otras entidades
    factura.associate=function(models){
        //Relacion con VENTA 1 a 1
        factura.belongsTo(models.venta,{foreignKey: 'id_venta'});
        //Relacion con PERSONA 1 a N
        factura.belongsTo(models.persona, {foreignKey: 'id_persona'});
    }
    return factura;
}