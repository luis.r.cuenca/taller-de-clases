'use strict'
module.exports=(sequelize, DataTypes)=>{
    //Datos para guardar en la entidad
    const persona= sequelize.define('persona',{
        tipo_identificacion: {type: DataTypes.ENUM("CEDULA","PASAPORTE","RUC"), defaultValue: "CEDULA"},
        identificacion: {type: DataTypes.STRING(20), defaultValue: "NO_DATA", unique : true},
        nombres: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
        apellidos: {type: DataTypes.STRING(100), defaultValue: "NO_DATA"},
        telefono: {type: DataTypes.STRING(15), defaultValue: "NO_DATA"},
        direccion: {type: DataTypes.STRING(200), defaultValue: "NO_DATA"},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        external_id:{type: DataTypes.UUID, defaultValue: DataTypes.UUID}
    },{freezeTableName: true});

    //Relaciones con la otras entidades
    persona.associate=function(models){
        //Relacion con CUENTA 1 a 1
        persona.hasOne(models.cuenta, {foreignKey: 'id_persona', as: 'cuenta'});
        //Relacion con ROL 1 a N
        persona.belongsTo(models.rol, {foreignKey: 'id_rol'});
        //Relacion con FACTURA 1 a N
        persona.hasMany(models.factura,{foreignKey: 'id_persona', as: 'factura'});
    }
    return persona;
}