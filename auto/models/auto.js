'use strict'
module.exports = (sequelize, DataTypes) => {
    //Datos para guardar en la entidad
    const auto = sequelize.define('auto', {
        modelo: { type: DataTypes.STRING(50), allowNull: false },
        anio: { type: DataTypes.DATE(), defaultValue:DataTypes.NOW},
        cilindraje: { type: DataTypes.STRING(50), allowNull: false },
        color: { type: DataTypes.ENUM("BLACO","ROJO", "NEGRO", "AZUL", "VERDE", "MORADO")},
        placa: { type: DataTypes.STRING(50), allowNull: false },
        precio: { type: DataTypes.DOUBLE(), defaultValue: 0.0},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });

    // Relacion con marca
    auto.associate = function (models) {
        //Relacion con MARCAS 1 a N
        auto.belongsTo(models.marcas, { foreignKey: 'id_marcas' });
        //Relacion con VENTA 1 a N
        auto.belongsTo(models.venta, { foreignKey: 'id_venta' });
    }
    return auto;
}