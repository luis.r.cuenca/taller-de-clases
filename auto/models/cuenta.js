'use strict'
module.exports=(sequelize, DataTypes)=>{
     //Datos para guardar en la entidad
    const cuenta= sequelize.define('cuenta',{
        usuario: {type: DataTypes.STRING(50), allowNull: false},
        clave: {type: DataTypes.STRING(100), allowNull: false},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    },{freezeTableName: true});

    // Relacion con persona
    cuenta.associate=function(models){
        //Relacion con PERSONA 1 a 1
        cuenta.belongsTo(models.persona,{foreignKey: 'id_persona'});
    }
    return cuenta;
}