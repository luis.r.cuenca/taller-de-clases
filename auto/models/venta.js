'use strict'

const { UUIDV4, DOUBLE } = require("sequelize");
module.exports=(sequelize, DataTypes)=>{
    //Datos para guardar en la entidad
    const venta = sequelize.define('venta',{
        valor: {type: DataTypes.DOUBLE(), defaultValue: 0.0},
        fecha: {type: DataTypes.DATE(), defaultValue:DataTypes.NOW},
        total: {type: DataTypes.DOUBLE(), defaultValue: 0.0},
        estado: {type: DataTypes.BOOLEAN, defaultValue: true},
        external_id:{type: DataTypes.UUID, defaultValue: DataTypes.UUID}
    },{freezeTableName: true});

    //Relaciones con la otras entidades
    venta.associate=function(models){
        //Relacion con FACTURA 1 a 1
        venta.hasOne(models.factura, {foreignKey: 'id_venta', as: 'factura'});
        //Relacion con AUTO 1 a N
        venta.hasMany(models.auto, {foreignKey: 'id_venta', as: 'auto'});
    }
    return venta;
}