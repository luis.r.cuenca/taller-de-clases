'use strict'
module.exports = (sequelize, DataTypes) => {
    //Datos para guardar en la entidad
    const marcas = sequelize.define('marcas', {
        nombre: { type: DataTypes.STRING(50), allowNull: false },
        anio: { type: DataTypes.STRING(50), allowNull: false },
        fechaLanzamiento: { type: DataTypes.DATE(), defaultValue: DataTypes.NOW },
        estado: { type: DataTypes.BOOLEAN, defaultValue: true },
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }
    }, { freezeTableName: true });

    // Relacion con marca
    marcas.associate = function (models) {
        //Relacion con AUTO 1 a N
        marcas.hasMany(models.auto, { foreignKey: 'id_marcas', as: 'auto' });
    }
    return marcas;
}