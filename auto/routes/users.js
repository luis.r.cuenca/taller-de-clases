var express = require('express');
var router = express.Router();
// Para validar los datos
const { body, validationResult } = require('express-validator');
//Controlador de ROL
const RolController = require('../controls/RolController');
var rolController = new RolController();
//Controlador de PERSONA
const PersonaController = require('../controls/PersonaController');
var personaController = new PersonaController();
// Controlador de CUENTA-CONTROLADOR
const CuentaController = require('../controls/CuentaController');
var cuentaController = new CuentaController();
// Controlador de Marcas
const MarcaController = require('../controls/MarcaController');
var marcaController = new MarcaController();
// Controlador de Auto
const AutoController = require('../controls/AutoController');
var autoController = new AutoController();

//Para el MIDDLEWARE
let jwt = require("jsonwebtoken");

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.json({ "version": "1.0", "name": "auto" });
});

//CUENTA
router.post('/sesion', [
  body('email', 'Ingrese correo válido').trim().exists().not().isEmpty().isEmail(),
  body('clave', 'Ingrese la clave').trim().exists()
], cuentaController.sesion);
//FIN CUENTA


// Listar datos de los roles
router.get('/roles', rolController.listar);

// DATOS DE PERSONA
// Guardar y validar datos de personas
router.post('/persona/guardar', [
  body('apellidos', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor a 2 y menor a 100"),
  body('nombres', 'Ingrese algun dato').exists()
], personaController.guardar);
// Guardar datos modificados
router.post('/persona/modificar', personaController.modificar);
// Listar datos de personas
//router.get('/persona'/*, auth*/, personaController.listar);
// Obtener datos de personas
router.post('/persona/obtener/:external', personaController.obtener);

//DATOS DE MARCA
router.post('/marca/guardar', [
  body('nombre', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor a 2 y menor a 100"),
  body('anio', 'Ingrese algun dato').exists()
], marcaController.guardar);

//DATOS DE AUTO
router.post('/auto/guardar', [
  body('modelo', 'Ingrese algun dato').trim().exists().not().isEmpty().isLength({ min: 3, max: 100 }).withMessage("Ingrese un valor mayor a 2 y menor a 100"),
  body('cilindraje', 'Ingrese algun dato').exists()
], autoController.guardar);

//MIDDLEWARE
var auth = async function middleware(re, res, next) {
  const token = req.headers['x-api-token'];
  if (token) {
    require("dotenv").config();
    const llave = process.env.KEY_SQ;
    jwt.verify(token.llave, async (err, decoded) => {
      if (err) {
        res.status(401);
        res.json({ msg: "Token no valido..!!", code: 401 });
      } else {
        var models = require('../models');
        var cuenta = models.cuenta;
        req.decoded = decoded;
        let aux = await cuenta.findOne({ where: { external_id: rec.decoded.external_id } });
        if (aux === null) {
          res.status(401);
          res.json({ msg: "Token no valido..!!", code: 401 });
        } else {
          next();
        }
      }
    });
  } else {
    res.status(401);
    res.json({ msg: "No existe Token", code: 401 });
  }
}

/*
router.get('/sumar/:a/:b', function(req, res, next) {
  var a= req.params.a*1;
  var b= Number(req.params.b);
  var c= a+b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});

router.post('/sumars', function(req, res, next) {
  var a= Number(req.body.a);
  var b= Number(req.body.b);
  if(isNaN(a)||isNaN(b)){
    res.status(300);
    res.json({'msg':"FALTAN DATOS"});
  }
  console.log(b);
  var c= a+b;
  res.status(200);
  res.json({"msg":"OK","resp":c});
});*/

module.exports = router;
